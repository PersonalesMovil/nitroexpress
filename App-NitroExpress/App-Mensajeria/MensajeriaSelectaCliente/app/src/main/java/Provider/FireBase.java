package Provider;

import android.util.Log;

import com.firebase.client.Firebase;

/**
 * Created by Daniel on 31/08/16.
 */
public class FireBase {

    String urlBase;
    Firebase myFireBase;

    public FireBase(){
        urlBase = "https://mensajeriaselecta.firebaseio.com";
    }

    public Firebase obtenerURLCategorias(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Categorias);
        return myFireBase;
    }

    public Firebase obtenerURLFavoritos(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Favoritos);
        return myFireBase;
    }

    public Firebase obtenerURLHistorial(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Historial);
        return myFireBase;
    }

    public Firebase obtenerURLPaises(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Paises);
        return myFireBase;
    }

    public Firebase obtenerURLPuntos(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Puntos);
        return myFireBase;
    }

    public Firebase obtenerURLRutas(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Ruta);
        return myFireBase;
    }

    public Firebase obtenerURLServicios(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Servicios);
        return myFireBase;
    }

    public Firebase obtenerURLTarjetas(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Tarjeta);
        return myFireBase;
    }

    public Firebase obtenerURLUsuarios(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Usuario);
        return myFireBase;
    }

}
