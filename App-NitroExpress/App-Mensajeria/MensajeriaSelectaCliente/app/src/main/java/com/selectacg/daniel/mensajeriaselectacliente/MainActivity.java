package com.selectacg.daniel.mensajeriaselectacliente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import Controllers.Comunicado.AyudaController;
import Controllers.Comunicado.ConfiguracionController;
import Controllers.Pagos.DetalleTarjetaController;
import Controllers.Promociones.DescuentosController;
import Controllers.Promociones.MisPuntosController;
import Controllers.Servicio.ConsultaServicioController;
import Controllers.Servicio.EstimacionServicioController;
import Controllers.Servicio.HistorialController;
import Controllers.Pagos.NuevaTarjetaController;
import Controllers.Autenticacion.RegistroController;
import Controllers.Autenticacion.RestablecerClaveController;
import Controllers.Pagos.TarjetaController;
import Controllers.Servicio.ImagesVideosController;
import Controllers.Servicio.MapaController;
import Enum.FragmentValues;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };

        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displayView(FragmentValues.GoogleMaps, new Bundle());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.nav_mapa){
            displayView(FragmentValues.GoogleMaps, new Bundle());
        } else if (id == R.id.nav_pagos) {
            displayView(FragmentValues.Tarjeta, new Bundle());
        } else if (id == R.id.nav_historial) {
            displayView(FragmentValues.Historial, new Bundle());
        } else if (id == R.id.nav_descuentos) {
            displayView(FragmentValues.Descuentos, new Bundle());
        } else if (id == R.id.nav_mis_puntos) {
            displayView(FragmentValues.Mispuntos, new Bundle());
        } else if (id == R.id.nav_ayuda) {
            displayView(FragmentValues.Ayuda, new Bundle());
        } else if (id == R.id.nav_configuracion) {
            displayView(FragmentValues.Configuracion, new Bundle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawers();
        return true;
    }

    public void displayView(String viewFragment, Bundle arguments) {
        try {
            Fragment fragment = null;
            String title = getString(R.string.app_name);
            switch (viewFragment) {
                case FragmentValues.ConsultaServicio:
                    //Esta es la vista principal de itinerarios
                    fragment = ConsultaServicioController.newInstance(arguments);
                    title = getString(R.string.title_consultation_service);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.EstimacionServicio:
                    //Esta es la vista que permite ver las opciones de impresion de la lectura
                    fragment = EstimacionServicioController.newInstance(arguments);
                    title = getString(R.string.title_estimating_service);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Historial:
                    //Esta es la vista que permite a un supervisor una serie de procesos
                    fragment = HistorialController.newInstance(arguments);
                    title = getString(R.string.title_record);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.NuevaTarjeta:
                    //Este metodo se utiliza para cerrar la session y volver a la vista principal
                    fragment = NuevaTarjetaController.newInstance(arguments);
                    title = getString(R.string.title_new_card);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Registro:
                    //Esta es la vista para buscar el medidor
                    fragment = RegistroController.newInstance(arguments);
                    title = getString(R.string.title_registry);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.RestablecerClave:
                    //Esta es la vista que muestra la descripcion del medidor buscado
                    fragment = RestablecerClaveController.newInstance(arguments);
                    title = getString(R.string.title_reset_key);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Tarjeta:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = TarjetaController.newInstance(arguments);
                    title = getString(R.string.title_card);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.DetalleTarjeta:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = DetalleTarjetaController.newInstance(arguments);
                    title = getString(R.string.title_card);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.GoogleMaps:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = MapaController.newInstance(arguments);
                    title = getString(R.string.title_location);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Descuentos:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = DescuentosController.newInstance(arguments);
                    title = getString(R.string.title_discount);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Mispuntos:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = MisPuntosController.newInstance(arguments);
                    title = getString(R.string.title_my_points);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Ayuda:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = AyudaController.newInstance(arguments);
                    title = getString(R.string.title_help);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.Configuracion:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = ConfiguracionController.newInstance(arguments);
                    title = getString(R.string.title_card);
                    getSupportActionBar().show();
                    break;
                case FragmentValues.camaraVideo:
                    //Esta es la vista que muestra las estadisticas del lector
                    fragment = ImagesVideosController.newInstance(arguments);
                    title = getString(R.string.title_images);
                    getSupportActionBar().show();
                    break;

            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(title);
            }
        } catch (Exception ex) {
            Log.e("DisplayFragment", ex.getMessage());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
