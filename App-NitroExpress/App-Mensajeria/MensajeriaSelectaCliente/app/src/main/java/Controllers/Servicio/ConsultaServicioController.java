package Controllers.Servicio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import Enum.FragmentValues;

import com.selectacg.daniel.mensajeriaselectacliente.MainActivity;
import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 11/06/16.
 */
public class ConsultaServicioController extends Fragment {

    EditText dirOrigenEditText;
    EditText dirDestinoEditText;
    Spinner categoriaSpinner;
    EditText anchoEditText;
    EditText alturaEditText;
    EditText pesoEditText;
    EditText descripcionEditText;
    ImageView tomarFotoImageView;
    Button estimarCostoButton;
    View rootView;

    public static ConsultaServicioController newInstance(Bundle arguments) {
        ConsultaServicioController fragment = new ConsultaServicioController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.consulta_servicio, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
            loadArguments();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.dirOrigenEditText = (EditText)rootView.findViewById(R.id.dirOrigenEditText);
        this.dirDestinoEditText = (EditText)rootView.findViewById(R.id.dirDestinoEditText);
        this.anchoEditText= (EditText)rootView.findViewById(R.id.anchoEditText);
        this.alturaEditText= (EditText)rootView.findViewById(R.id.alturaEditText);
        this.pesoEditText= (EditText)rootView.findViewById(R.id.pesoEditText);
        this.descripcionEditText= (EditText)rootView.findViewById(R.id.descripcionEditText);
        this.tomarFotoImageView = (ImageView) rootView.findViewById(R.id.tomarFotoImageView);
        this.estimarCostoButton= (Button) rootView.findViewById(R.id.estimarCostoButton);
        this.categoriaSpinner= (Spinner) rootView.findViewById(R.id.categoriaSpinner);
    }

    private void loadEventButton(){
        tomarFotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                ((MainActivity) getActivity()).displayView(FragmentValues.camaraVideo, arguments);
            }
        });

        estimarCostoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void loadArguments(){
        try {
            if (getArguments() != null) {
                dirOrigenEditText.setText(getArguments().getString("dirOrigen"));
                dirDestinoEditText.setText(getArguments().getString("dirDestino"));
            }

            String categorias[] = {"Delicado","Pesado","Grande","Otros"};
            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item,categorias);
            categoriaSpinner.setAdapter(spinnerArrayAdapter);
        }catch (Exception e){
            Log.d("LoadArguments", "Error: " + e.getLocalizedMessage());
        }
    }
}
