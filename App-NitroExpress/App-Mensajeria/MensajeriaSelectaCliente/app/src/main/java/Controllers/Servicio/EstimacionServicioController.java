package Controllers.Servicio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 11/06/16.
 */
public class EstimacionServicioController extends Fragment {

    TextView costoEnvioTextView;
    TextView addressOrigenTextView;
    TextView addressDestinoTextView;
    TextView categoryTextView;
    TextView anchoTextView;
    TextView alturaTextView;
    TextView pesoTextView;
    TextView descripcionTextView;
    Button goBackButton;
    Button disponiblityButton;
    View rootView;

    public static EstimacionServicioController newInstance(Bundle arguments) {
        EstimacionServicioController fragment = new EstimacionServicioController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.estimacion_servicio, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.costoEnvioTextView = (TextView)rootView.findViewById(R.id.costoEnvioTextView);
        this.addressOrigenTextView = (TextView)rootView.findViewById(R.id.addressOrigenTextView);
        this.addressDestinoTextView = (TextView)rootView.findViewById(R.id.addressDestinoTextView);
        this.categoryTextView = (TextView)rootView.findViewById(R.id.categoryTextView);
        this.anchoTextView = (TextView)rootView.findViewById(R.id.anchoTextView);
        this.alturaTextView = (TextView)rootView.findViewById(R.id.alturaTextView);
        this.pesoTextView = (TextView)rootView.findViewById(R.id.pesoTextView);
        this.descripcionTextView = (TextView)rootView.findViewById(R.id.descripcionTextView);
        this.goBackButton = (Button) rootView.findViewById(R.id.goBackButton);
        this.disponiblityButton = (Button) rootView.findViewById(R.id.disponiblityButton);
    }

    private void loadEventButton(){
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        disponiblityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

}
