package Controllers.Promociones;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 1/09/16.
 */
public class MisPuntosController extends Fragment {

    View rootView;

    public static MisPuntosController newInstance(Bundle arguments) {
        MisPuntosController fragment = new MisPuntosController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.mis_puntos, container, false);
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }
}

