package Controllers.Autenticacion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 10/06/16.
 */
public class RestablecerClaveController extends Fragment{

    EditText emailEditText;
    Button reestablishingPasswordButton;
    View rootView;

    public static RestablecerClaveController newInstance(Bundle arguments) {
        RestablecerClaveController fragment = new RestablecerClaveController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.restablecer_password, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView) {
        this.emailEditText = (EditText) rootView.findViewById(R.id.emailEditText);
        this.reestablishingPasswordButton = (Button) rootView.findViewById(R.id.reestablishingPasswordButton);
    }

    private void loadEventButton(){
        reestablishingPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
