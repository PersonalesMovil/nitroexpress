package Controllers.Autenticacion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.selectacg.daniel.mensajeriaselectacliente.R;

public class RegistroController extends Fragment {

    View rootView;
    Button facebookButton;
    Button googleButton;
    EditText nameEditText;
    EditText lastNameEditText;
    EditText emailEditText;
    EditText indicativeEditText;
    EditText numberTelEditText;
    EditText passwordEditText;
    Button nextButton;

    public RegistroController() {
    }

    public static RegistroController newInstance(Bundle args) {
        RegistroController fragment = new RegistroController();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.registro_usuario, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.facebookButton = (Button) rootView.findViewById(R.id.facebookButton);
        this.googleButton = (Button) rootView.findViewById(R.id.googleButton);
        this.nextButton = (Button) rootView.findViewById(R.id.nextButton);
        this.nameEditText = (EditText)rootView.findViewById(R.id.nameEditText);
        this.lastNameEditText = (EditText)rootView.findViewById(R.id.lastNameEditText);
        this.passwordEditText = (EditText) rootView.findViewById(R.id.passwordEditText);
        this.indicativeEditText = (EditText) rootView.findViewById(R.id.indicativeEditText);
        this.numberTelEditText = (EditText) rootView.findViewById(R.id.numberTelEditText);
        this.emailEditText = (EditText) rootView.findViewById(R.id.emailEditText);
    }

    private void loadEventButton(){
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }




}
