package Controllers.Servicio;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import Enum.FragmentValues;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.selectacg.daniel.mensajeriaselectacliente.MainActivity;
import com.selectacg.daniel.mensajeriaselectacliente.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Model.Direccion;
import Resource.DirectionsJSONParser;
import Resource.GoogleMapsApiQuery;
import Resource.LocationGPS;

public class MapaController extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    View rootView;
    ArrayList<LatLng> markerPoints;
    LinearLayout filtroRutalinearLayout;
    TextView direccionOrigen;

    //Descripcion del envio una vez el usuario haya seleccionado las 2 direcciones
    LinearLayout descripcionDataLinearLayout;
    TextView dirOrigenTextView;
    TextView dirDestinoTextView;
    TextView distanciaTextView;
    TextView tiempoTextView;
    Button estimarCostoButton;
    Button editarButtton;


    public static MapaController newInstance(Bundle args) {
        MapaController fragment = new MapaController();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            FragmentManager fm = getChildFragmentManager();
            mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
            if (mapFragment == null) {
                mapFragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
            }
        } catch (Exception ex) {
            Log.d("OnCreate", "Error: " + ex.getMessage());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.activity_map, container, false);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            //SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            loadControllerFromView();
            loadEvents();
        } catch (Exception ex) {
            Log.d("loadData:", "Error " + ex.getMessage());
        }
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            cargarUbicacionActual();
        } catch (Exception ex) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap == null) {
            mMap = mapFragment.getMap();
            cargarUbicacionActual();
        }
    }

    private void cargarUbicacionActual() {
        try {// Add a marker in Sydney and move the camera
            LocationGPS location = new LocationGPS(getActivity());
            LatLng currentLocationGPS = location.getGPS();
            if (currentLocationGPS != null) {

                markerPoints = new ArrayList<LatLng>();
                LatLng currentLocation = new LatLng(currentLocationGPS.latitude, currentLocationGPS.longitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16), 2000, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });

                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                mMap.setTrafficEnabled(false);

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return false;
                    }
                });

                mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {

                    }
                });

                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        try {
                            // Already two locations
                            if (markerPoints.size() > 1) {
                                markerPoints.clear();
                                mMap.clear();
                                descripcionDataLinearLayout.setVisibility(View.INVISIBLE);
                            }

                            // Adding new item to the ArrayList
                            markerPoints.add(latLng);

                            // Creating MarkerOptions
                            MarkerOptions options = new MarkerOptions();

                            // Setting the position of the marker
                            options.position(latLng);

                            /**
                             * For the start location, the color of marker is GREEN and
                             * for the end location, the color of marker is RED.
                             */
                            if (markerPoints.size() == 1) {
                                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            } else if (markerPoints.size() == 2) {
                                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                            }

                            // Add new marker to the Google Map Android API V2
                            mMap.addMarker(options);

                            // Checks, whether start and end locations are captured
                            if (markerPoints.size() >= 2) {
                                LatLng origin = markerPoints.get(0);
                                LatLng dest = markerPoints.get(1);

                                // Getting URL to the Google Directions API
                                GoogleMapsApiQuery googleMapsApiQuery = new GoogleMapsApiQuery();
                                String url = googleMapsApiQuery.getDirectionsUrl(origin, dest);

                                DownloadTask downloadTask = new DownloadTask();
                                // Start downloading json data from Google Directions API
                                downloadTask.execute(url);
                            }
                        } catch (Exception e) {
                            Log.d("Error", e.getMessage());
                        }
                    }
                });

                mMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
                    @Override
                    public void onPolylineClick(Polyline polyline) {
                        Bundle arguments = new Bundle();
                        arguments.putString("dirOrigen",dirOrigenTextView.getText().toString());
                        arguments.putString("dirDestino",dirDestinoTextView.getText().toString());
                        arguments.putString("distancia",distanciaTextView.getText().toString());
                        arguments.putString("tiempo",tiempoTextView.getText().toString());
                        ((MainActivity) getActivity()).displayView(FragmentValues.ConsultaServicio, arguments);
                    }
                });

            } else {
                Toast.makeText(getActivity(), "No se pudo encontrar la Geoposicion", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception ex) {
            Log.d("Error map", ex.getMessage());
        }
    }

    private void loadControllerFromView(){
        filtroRutalinearLayout = (LinearLayout)rootView.findViewById(R.id.filtroRutaLinearLayout);
        direccionOrigen = (TextView) rootView.findViewById(R.id.direccionPartidaTextView);

        descripcionDataLinearLayout = (LinearLayout) rootView.findViewById(R.id.descripcionDataLinearLayout);
        dirOrigenTextView = (TextView) rootView.findViewById(R.id.dirOrigenTextView);
        dirDestinoTextView = (TextView) rootView.findViewById(R.id.dirDestinoTextView);
        distanciaTextView = (TextView) rootView.findViewById(R.id.distanciaTextView);
        tiempoTextView = (TextView) rootView.findViewById(R.id.tiempoTextView);
        estimarCostoButton = (Button) rootView.findViewById(R.id.estimarCostoButton);
        editarButtton = (Button) rootView.findViewById(R.id.editarButtton);

        descripcionDataLinearLayout.setVisibility(View.INVISIBLE);

    }

    private void loadEvents(){
        Button buttonMapNormal = (Button) rootView.findViewById(R.id.buttonMapNormal);
        buttonMapNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });

        Button buttonMapHibryd = (Button) rootView.findViewById(R.id.buttonMapHibryd);
        buttonMapHibryd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            }
        });

        Button buttonMapSatelital = (Button) rootView.findViewById(R.id.buttonMapSatelital);
        buttonMapSatelital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            }
        });

        final CheckBox traficoCheck = (CheckBox)rootView.findViewById(R.id.traficoCheck);
        traficoCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(traficoCheck.isChecked()){
                    mMap.setTrafficEnabled(true);
                }else{
                    mMap.setTrafficEnabled(false);
                }
            }
        });

        filtroRutalinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        estimarCostoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                arguments.putString("dirOrigen",dirOrigenTextView.getText().toString());
                arguments.putString("dirDestino",dirDestinoTextView.getText().toString());
                arguments.putString("distancia",distanciaTextView.getText().toString());
                arguments.putString("tiempo",tiempoTextView.getText().toString());
                ((MainActivity) getActivity()).displayView(FragmentValues.ConsultaServicio, arguments);
            }
        });

        editarButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markerPoints.clear();
                mMap.clear();
                descripcionDataLinearLayout.setVisibility(View.INVISIBLE);
            }
        });
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("downloading url", "Error: " + e.getLocalizedMessage());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        Direccion directionsResult;


        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            directionsResult = new Direccion();
            directionsResult.coordenadas = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                directionsResult = parser.parseDirections(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return directionsResult.coordenadas;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.RED);
                lineOptions.clickable(true);
            }
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            descripcionDataLinearLayout.setVisibility(View.VISIBLE);
            dirOrigenTextView.setText(directionsResult.direccion_origen);
            dirDestinoTextView.setText(directionsResult.direccion_destino);
            distanciaTextView.setText(directionsResult.distancia);
            tiempoTextView.setText(directionsResult.tiempo_entre_distancia);
        }
    }
}
