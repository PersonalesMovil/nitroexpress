package Controllers.Servicio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.util.ArrayList;

import Adapters.HistorialAdapter;
import Model.Historial;
import Model.Ruta;
import Model.Tarjeta;

/**
 * Created by Daniel on 11/06/16.
 */
public class HistorialController extends Fragment {

    ListView historialeslistView;
    HistorialAdapter adapter;
    View rootView;

    public static HistorialController newInstance(Bundle arguments) {
        HistorialController fragment = new HistorialController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.lista_historiales, container, false);
            loadControllersFromView(rootView);
            loadData();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.historialeslistView = (ListView) rootView.findViewById(R.id.historialeslistView);
    }

    private void loadData(){
        ArrayList<Historial> listHistorial = new ArrayList<>();
        Ruta ruta = new Ruta();
        Tarjeta tarjetaCobro = new Tarjeta();

        Historial historial = new Historial();
        historial.setCalificacion("5");
        historial.setCategoria("Delicado");
        historial.setFecha("24/12/2016 10:30");
        historial.setId_historial("123131s312312");
        historial.setNameCourier("Samid Ortiz");
        historial.setRuta(ruta);
        historial.setTarjetaCobro(tarjetaCobro);
        historial.setTotalViaje("12.000 COP");

        adapter = new HistorialAdapter(getContext(),listHistorial ,new ArrayList<Historial>());
        historialeslistView.setAdapter(adapter);
    }
}
