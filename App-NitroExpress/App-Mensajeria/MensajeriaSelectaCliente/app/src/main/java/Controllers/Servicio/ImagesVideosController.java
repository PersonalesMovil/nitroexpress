package Controllers.Servicio;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;
import Enum.TypePhoto;
import AsyncTask.BitmapWorkerTask;
import Model.Fotos;
import Status.UserConnect;

import com.selectacg.daniel.mensajeriaselectacliente.CameraActivity;
import com.selectacg.daniel.mensajeriaselectacliente.MainActivity;
import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by SelectaCG on 14/07/15.
 */

public class ImagesVideosController extends Fragment {

    // Activity result key for camera
    static final int REQUEST_TAKE_PHOTO = 11111;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;

    Toolbar tb;

    ArrayList<Fotos> listFotos;
    ArrayList<CameraActivity> listActivityImages;
    ArrayList<CameraActivity> listActivityVideo;
    ArrayList<ImageView> listImagesImageView;
    ArrayList<VideoView> listVideosVideoView;
    ArrayList<LinearLayout> listlinealLinearLayout;
    HorizontalScrollView tblcontenedor;
    private int countImageCurrent;
    private int countVideoCurrent;
    private Boolean isCam;
    RelativeLayout contentPage;
    RelativeLayout contentLoadSmp;
    LinearLayout linealNuevo;
    View rootView;
    Boolean new_meter_found;
    Boolean new_anomaly;
    String imageFileNameCurrent;

    public ImagesVideosController(){
        super();
        listActivityImages = new ArrayList<>();
        listActivityVideo = new ArrayList<>();
        listImagesImageView = new ArrayList<>();
        listVideosVideoView = new ArrayList<>();
        listlinealLinearLayout = new ArrayList<>();
        listFotos = new ArrayList<>();
        countImageCurrent = 0;
        countVideoCurrent = 0;
        isCam = false;
    }

    public static ImagesVideosController newInstance(Bundle arguments) {
        ImagesVideosController fragment = new ImagesVideosController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.images_videos, container, false);
        contentPage = (RelativeLayout) rootView.findViewById(R.id.contentPage);
        contentLoadSmp = (RelativeLayout) rootView.findViewById(R.id.contentLoadSMP);
        contentPage.setVisibility(View.VISIBLE);
        contentLoadSmp.setVisibility(View.INVISIBLE);
        try {

            tblcontenedor = (HorizontalScrollView) rootView.findViewById(R.id.contenedorImagesVideosTableLayout);
            loadArguments();
            capturePhoto();
        }catch (Exception ex){
            Log.d("ErrorImage", "OnCreateView:" + ex.getMessage());
        }
        return rootView;
    }

    private void loadArguments(){
        try {
            if (getArguments() != null) {
                UserConnect dataUser = UserConnect.newInstance();
                listFotos = new ArrayList<>();
                if (listFotos.size() > 0) {
                    loadImagesSaved();
                }
            }
        }catch (Exception ex){
            Log.d("Error", ex.getMessage());
        }
    }

    protected void dispatchTakeVideoIntent() {
        try{
            isCam = false;
            // checkea si existe una camara en el dispositivo
            Context context = getActivity();
            PackageManager packageManager = context.getPackageManager();
            if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
                Toast.makeText(getActivity(), "El dispositivo no tiene camara", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            // Si la camara existe, genera el proceso
            Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            CameraActivity activity = new CameraActivity();
            // if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go.
            // If you don't do this, you may get a crash in some devices.
            File videoFile = null;
            try {
                videoFile = createVideoFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(getActivity(), "No se pudo guardar el video correctamente...", Toast.LENGTH_SHORT);
                toast.show();
            }
            // Continue only if the File was successfully created
            if (videoFile != null) {
                Uri fileUri = Uri.fromFile(videoFile);
                activity.setCapturedImageURI(fileUri);
                activity.setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        activity.getCapturedImageURI());
                listActivityVideo.add(activity);
                countVideoCurrent++;
                startActivityForResult(takePictureIntent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
            }
            //}
        }catch (Exception ex){
            String strError = ex.getMessage();
            System.out.println(strError);
        }
    }

    protected void dispatchTakePictureIntent() {
        try{
            isCam = true;
            // checkea si existe una camara en el dispositivo
            Context context = getActivity();
            PackageManager packageManager = context.getPackageManager();
            if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
                Toast.makeText(getActivity(), "El dispositivo no tiene camara", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            // Si la camara existe, genera el proceso
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            CameraActivity activity = new CameraActivity();
            //if (takePictureIntent != null) {
                // Create the File where the photo should go.
                // If you don't do this, you may get a crash in some devices.
            File photoFile = null;
            try {
                String timeStamp = new SimpleDateFormat(getString(R.string.format_date_image)).format(new Date());
                String name = "Nombre_imagen_" + timeStamp ;
                photoFile = createImageFile(name);
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri fileUri = Uri.fromFile(photoFile);
                    activity.setCapturedImageURI(fileUri);
                    activity.setCurrentPhotoPath(fileUri.getPath());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            activity.getCapturedImageURI());
                    listActivityImages.add(activity);
                    countImageCurrent++;
                    try {
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }catch (Exception ex){
                        Log.d("action cam", ex.getMessage());
                    }
                }

            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(getActivity(), "No se pudo guardar la foto correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            //}
        }catch (Exception ex){
            Log.d("ErrorImage", "DispcherTakePhoto:" + ex.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                try {
                    addPhotoVideoToGallery(1);
                }catch (Exception ex){

                }
                try {
                    loadImageRequest(false, null);
                }catch (Exception ex){

                }
            }else if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                loadVideoRequest();
            } else {
                if(!isCam) {
                    Toast.makeText(getActivity(), "No se grabo el video", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    Toast.makeText(getActivity(), "No se capturo la imagen", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }catch (Exception ex){
            Log.d("ErrorImage", "OnResultActivity:" + ex.getMessage());
        }
    }

    private void setFullImageFromFilePath(String imagePath, ImageView imageView, Boolean isLoadInit){
        try {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView, imagePath);
            task.execute(0);
            String nombreFichero = imageFileNameCurrent;
            Fotos fotos = new Fotos();
            fotos.setIdInt("1_"+nombreFichero);
            fotos.setNombreFichero(nombreFichero);
            fotos.currentPATH = imagePath;
            if (!containsPhotoInList(listFotos, fotos)) {
                listFotos.add(fotos);
            }
        }catch (Exception ex){
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void setFullVideoFromFilePath(String imagePath, VideoView videoView){
        videoView.setVideoPath(imagePath);
    }

    protected File createImageFile(String name) throws IOException {
        try {
            // Create an image file name
            imageFileNameCurrent = name;
            File storageDir = Environment.getExternalStoragePublicDirectory(TypePhoto.FOLDER_TO_SAVE);
            if (storageDir.exists()) {
                File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                return image;
            }else{
                storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                if (storageDir.exists()) {
                    File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                    return image;
                }else{
                    storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    if (storageDir.exists()) {
                        File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                        return image;
                    }else{
                        Toast.makeText(getActivity(), "Debe crear una carpeta llamada" + TypePhoto.FOLDER_TO_SAVE, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            //Save a file: path for use with ACTION_VIEW intents
            //listActivityImages.get(countImageCurrent).setCurrentPhotoPath("file:" + image.getAbsolutePath());
            return null;
        }catch ( Exception ex){
            Log.d("Error","createImageFile:" + ex.getMessage());
        }
        return null;
    }

    protected File createVideoFile() throws IOException {
        File image;
        try {
            // Create an image file name
            String timeStamp = new SimpleDateFormat(getString(R.string.format_date_image)).format(new Date());
            String imageFileName = "VID_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".mp4",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            //CameraActivity activity = (CameraActivity)getActivity();
            //activity.setCurrentPhotoPath("file:" + image.getAbsolutePath());
        }catch (Exception ex){
            Log.d("load video",ex.getMessage());
        }
        return null;
    }

    protected void addPhotoVideoToGallery(int type) {
        try {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            //CameraActivity activity = (CameraActivity)getActivity();
            CameraActivity activity;
            if(type == 1) {
                 activity = listActivityImages.get(countImageCurrent - 1);
            }else{
                activity = listActivityVideo.get(countVideoCurrent - 1);
            }
            File f = new File(activity.getCurrentPhotoPath());
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.getActivity().sendBroadcast(mediaScanIntent);
        }catch (Exception ex){
            Log.d("Error", "AddPhotoToGallery:" + ex.getMessage());
        }
    }

    private Boolean capturePhoto(){
        try {
            long spaceMinToSave = 10485760;
            long spaceAvailable = Environment.getDataDirectory().getFreeSpace();
            Log.i("Espacio disponible cel", Long.toString(spaceAvailable));
            long spaceAvailableSD = Environment.getExternalStorageDirectory().getFreeSpace();
            Log.i("Espacio disponible sd", Long.toString(spaceAvailableSD));
            long freeSpaceRootDirectory = Environment.getRootDirectory().getFreeSpace();
            String availableSD = Environment.getExternalStorageState();
            Boolean availableSDbit = false;
            if (availableSD.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
                availableSDbit = true;
            }
            if ((availableSDbit && spaceAvailableSD > spaceMinToSave) || (spaceAvailable > spaceMinToSave)) {
                if (listImagesImageView.size() < 3) {
                    dispatchTakePictureIntent();
                    return true;
                } else {
                    //Mensaje para indicar que a superado el limite de imagenes y videos
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(getString(R.string.msj_count_image_permited))
                            .setTitle(getString(R.string.msj_warning));
                    builder.setPositiveButton(getString(R.string.ctl_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            } else {
                //Mensaje para indicar que no hay espacio disponible en la memoria
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.msj_not_space_available))
                        .setTitle(getString(R.string.msj_warning));
                builder.setPositiveButton(getString(R.string.ctl_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        }catch (Exception ex){
            Log.d("save photo", ex.getMessage());
            return false;
        }
    }

    private void captureVideo(){
        if(listVideosVideoView.size() < 3) {
            dispatchTakeVideoIntent();
        }else{
            //Mensaje para indicar que a superado el limite de imagenes y videos
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.msj_count_image_permited))
                    .setTitle(getString(R.string.msj_confirmation));
            builder.setPositiveButton(getString(R.string.ctl_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private void loadVideoRequest(){
        try {
            addPhotoVideoToGallery(2);
            // Show the full sized image.
            LinearLayout linealNuevo;
            RelativeLayout.LayoutParams regla;
            if (listlinealLinearLayout.size() == 0) {

                linealNuevo = new LinearLayout(getActivity());
                regla = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500);
                linealNuevo.setOrientation(LinearLayout.HORIZONTAL);

                switch (listlinealLinearLayout.size()) {
                    case 0:
                        linealNuevo.setId(R.id.imageVideoLineal1);
                        //regla.addRule(RelativeLayout.BELOW, R.id.contenedorImagesVideosTableLayout);
                        regla.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        regla.addRule(RelativeLayout.CENTER_VERTICAL);
                        //regla.addRule(RelativeLayout.CENTER_IN_PARENT);
                        break;
                    case 1:
                        linealNuevo.setId(R.id.imageVideoLineal2);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal1);
                        break;
                    case 2:
                        linealNuevo.setId(R.id.imageVideoLineal3);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal2);
                        break;
                    case 3:
                        linealNuevo.setId(R.id.imageVideoLineal4);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal3);
                        break;
                    case 4:
                        linealNuevo.setId(R.id.imageVideoLineal5);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal4);
                        break;
                    case 5:
                        linealNuevo.setId(R.id.imageVideoLineal6);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal5);
                        break;
                }
                linealNuevo.setLayoutParams(regla);
                tblcontenedor.addView(linealNuevo);
            } else {
                linealNuevo = listlinealLinearLayout.get(0);
            }

            VideoView videoCurrent = new VideoView(this.getActivity().getApplicationContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 400);
            videoCurrent.setLayoutParams(layoutParams);
            videoCurrent.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (listVideosVideoView.contains(v)) {
                        int pos = listVideosVideoView.indexOf(v);
                        listVideosVideoView.remove(pos);
                        listActivityVideo.remove(pos);
                        listFotos.remove(pos);
                        countVideoCurrent--;
                        v.destroyDrawingCache();
                        v.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.msj_deleted_video_selected), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.msj_cannot_deleted_video_selected), Toast.LENGTH_SHORT)
                                .show();
                    }
                    return true;
                }
            });

            videoCurrent.getLayoutParams().height = 500;
            videoCurrent.getLayoutParams().width = 500;
            linealNuevo.addView(videoCurrent);
            listVideosVideoView.add(videoCurrent);
            setFullVideoFromFilePath(listActivityVideo.get(countVideoCurrent - 1).getCurrentPhotoPath(), listVideosVideoView.get(countVideoCurrent - 1));
        }catch (Exception ex){
            Toast.makeText(getActivity(), "error:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void loadImageRequest(Boolean isLoadInit, Fotos current){
        try {
            RelativeLayout.LayoutParams regla;
            if (listlinealLinearLayout.size() == 0) {
                linealNuevo = new LinearLayout(getActivity());
                regla = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                linealNuevo.setOrientation(LinearLayout.HORIZONTAL);
                switch (listlinealLinearLayout.size()) {
                    case 0:
                        linealNuevo.setId(R.id.imageVideoLineal1);
                        regla.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        regla.addRule(RelativeLayout.CENTER_VERTICAL);
                        break;
                    case 1:
                        linealNuevo.setId(R.id.imageVideoLineal2);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal1);
                        break;
                    case 2:
                        linealNuevo.setId(R.id.imageVideoLineal3);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal2);
                        break;
                    case 3:
                        linealNuevo.setId(R.id.imageVideoLineal4);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal3);
                        break;
                    case 4:
                        linealNuevo.setId(R.id.imageVideoLineal5);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal4);
                        break;
                    case 5:
                        linealNuevo.setId(R.id.imageVideoLineal6);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal5);
                        break;
                }
                //Esta funcion alinea la imagen al centro de la pantalla
                //regla.addRule(RelativeLayout.CENTER_VERTICAL);
                linealNuevo.setLayoutParams(regla);
                listlinealLinearLayout.add(linealNuevo);
                //Se añade el linealLayout al tableLayout
                tblcontenedor.addView(linealNuevo);
            } else {
                linealNuevo = listlinealLinearLayout.get(0);
            }

            try {
                ImageView imageCurrent = new ImageView(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400,400);
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                layoutParams.setMargins(10,50,10,0);
                imageCurrent.setLayoutParams(layoutParams);
                imageCurrent.setBackgroundColor(getResources().getColor(R.color.color_background_image));
                imageCurrent.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        try {
                            if (listImagesImageView.contains(v)) {
                                int pos = listImagesImageView.indexOf(v);
                                linealNuevo.removeView(v);
                                listImagesImageView.remove(pos);
                                listActivityImages.remove(pos);
                                listFotos.remove(pos);
                                countImageCurrent--;
                                Toast.makeText(getActivity(), getString(R.string.msj_deleted_image_selected), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.msj_cannot_deleted_image_selected), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }catch (Exception ex){
                            Log.d("Error delete image", ex.getMessage());
                        }
                        return true;
                    }
                });

                //Se añaden la imagen al linearLayout
                linealNuevo.addView(imageCurrent);
                listImagesImageView.add(imageCurrent);
                String path;
                if(current != null) {
                    path = current.currentPATH;
                }else{
                    path = listActivityImages.get(countImageCurrent - 1).getCurrentPhotoPath();
                }
                setFullImageFromFilePath(path, listImagesImageView.get(countImageCurrent - 1), isLoadInit);

            }catch (Exception ex){
                Log.d("save image", ex.getMessage());
            }
        }catch (Exception ex){
            Log.d("Error load image", ex.getMessage());
        }
    }

    private void loadImagesSaved() throws IOException {
        try {
            for (Fotos current : listFotos) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                CameraActivity activity = new CameraActivity();
                File photoFile = createImageFile(current.getNombreFichero());
                if (photoFile != null) {
                    Uri fileUri = Uri.fromFile(photoFile);
                    activity.setCapturedImageURI(fileUri);
                    activity.setCurrentPhotoPath(fileUri.getPath());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            activity.getCapturedImageURI());
                    listActivityImages.add(activity);
                    countImageCurrent++;
                    try {
                        loadImageRequest(true, current);
                    } catch (Exception ex) {
                        Log.d("load cam", ex.getMessage());
                    }
                }
            }
        }catch (Exception ex){
            Log.d("load cam",ex.getMessage());
        }
    }

    private Boolean containsPhotoInList(ArrayList<Fotos> listFotos, Fotos photo){
        for (Fotos current: listFotos){
               if (current.getNombreFichero().equalsIgnoreCase(photo.getNombreFichero()) &&
                       current.currentPATH.equalsIgnoreCase(photo.currentPATH)){
                   return true;
               }
        }
        return false;
    }
}

