package Controllers.Pagos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 10/06/16.
 */
public class NuevaTarjetaController extends Fragment {

    EditText numberTargetEditText;
    EditText MMEditText;
    EditText AAEditText;
    EditText CVVEditText;
    Spinner paisSpinner;
    Button registerButton;
    View rootView;

    public static NuevaTarjetaController newInstance(Bundle arguments) {
        NuevaTarjetaController fragment = new NuevaTarjetaController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.nueva_tarjeta, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.numberTargetEditText = (EditText)rootView.findViewById(R.id.numberTargetEditText);
        this.MMEditText = (EditText)rootView.findViewById(R.id.MMEditText);
        this.AAEditText = (EditText) rootView.findViewById(R.id.AAEditText);
        this.CVVEditText = (EditText) rootView.findViewById(R.id.CVVEditText);
        this.paisSpinner = (Spinner) rootView.findViewById(R.id.paisSpinner);
        this.registerButton = (Button) rootView.findViewById(R.id.registerButton);
    }

    private void loadEventButton(){
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
