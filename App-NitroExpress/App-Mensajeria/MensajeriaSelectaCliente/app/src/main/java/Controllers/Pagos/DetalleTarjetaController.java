package Controllers.Pagos;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.selectacg.daniel.mensajeriaselectacliente.R;

/**
 * Created by Daniel on 31/08/16.
 */
public class DetalleTarjetaController extends Fragment {

    EditText numberTargetEditText;
    EditText MMEditText;
    EditText AAEditText;
    EditText CVVEditText;
    Spinner  paisSpinner;
    Spinner  tipoTarjetaSpinner;
    ArrayAdapter<String> paisAdapter;
    ArrayAdapter<String> tipoTarjetaAdapter;

    Button deleteCardButton;
    View rootView;

    public static DetalleTarjetaController newInstance(Bundle arguments) {
        DetalleTarjetaController fragment = new DetalleTarjetaController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.nueva_tarjeta, container, false);
            loadControllersFromView(rootView);
            loadEventButton();
            loadData();
        } catch (Exception ex) {
            Log.d("loadData:", "Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView) {
        this.numberTargetEditText = (EditText) rootView.findViewById(R.id.numberTargetEditText);
        this.MMEditText = (EditText) rootView.findViewById(R.id.MMEditText);
        this.AAEditText = (EditText) rootView.findViewById(R.id.AAEditText);
        this.CVVEditText = (EditText) rootView.findViewById(R.id.CVVEditText);
        this.paisSpinner = (Spinner) rootView.findViewById(R.id.paisSpinner);
        this.tipoTarjetaSpinner = (Spinner) rootView.findViewById(R.id.tipoTarjetaSpinner);
        this.deleteCardButton = (Button) rootView.findViewById(R.id.deleteButton);
    }

    private void loadEventButton() {
        deleteCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void loadData(){
        try {
            if (getArguments() != null) {
                numberTargetEditText.setText(getArguments().getString("numero"));
                MMEditText.setText(getArguments().getString("mes"));
                AAEditText.setText(getArguments().getString("anio"));
                CVVEditText.setText(getArguments().getString("ccv"));

                paisAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_expandable_list_item_1);
                tipoTarjetaAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_expandable_list_item_1);

                paisSpinner.setAdapter(paisAdapter);
                tipoTarjetaSpinner.setAdapter(tipoTarjetaAdapter);
            }
        }catch (Exception ex){
            Log.d("LoadData","DetalleTarjeta Error: " + ex.getMessage());
        }
    }
}
