package Controllers.Pagos;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import Adapters.TarjetaAdapter;
import Enum.FragmentValues;
import Model.Tarjeta;

import com.selectacg.daniel.mensajeriaselectacliente.MainActivity;
import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.util.ArrayList;

/**
 * Created by Daniel on 11/06/16.
 */
public class TarjetaController extends Fragment {

    ListView tarjetasPagolistView;
    View rootView;
    TarjetaAdapter adapter;

    public static TarjetaController newInstance(Bundle arguments) {
        TarjetaController fragment = new TarjetaController();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.lista_tarjetas, container, false);
            loadControllersFromView(rootView);
            eventButtons();
            loadData();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
        return rootView;
    }

    private void loadControllersFromView(View rootView){
        this.tarjetasPagolistView = (ListView) rootView.findViewById(R.id.tarjetasPagolistView);
    }

    private void eventButtons(){
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).displayView(FragmentValues.NuevaTarjeta, new Bundle());

                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        tarjetasPagolistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 Tarjeta tarjetaSelected = adapter.getItem(position);
                adapter.clearSelection();
                adapter.setNewSelection(position);
                Bundle arguments = new Bundle();
                arguments.putString("numero", tarjetaSelected.getNumero());
                arguments.putString("mes", tarjetaSelected.getMM());
                arguments.putString("ano", tarjetaSelected.getAA());
                arguments.putString("ccv", tarjetaSelected.getCCV());
                arguments.putString("pais", tarjetaSelected.getPais());
                arguments.putString("tipo", tarjetaSelected.getDescripcion());
                ((MainActivity) getActivity()).displayView(FragmentValues.DetalleTarjeta, arguments);
            }
        });
    }

    private void loadData(){
        ArrayList<Tarjeta> listTarjeta = new ArrayList<>();
        Tarjeta tarjeta = new Tarjeta();
        tarjeta.setAA("17");
        tarjeta.setCCV("186");
        tarjeta.setDescripcion("Tarjeta personal para el ahorro");
        tarjeta.setNumero("1234 5678 6789");
        tarjeta.setPais("Colombia");
        listTarjeta.add(tarjeta);

        adapter = new TarjetaAdapter(getContext(), listTarjeta, new ArrayList<Tarjeta>());
        tarjetasPagolistView.setAdapter(adapter);
    }
}
