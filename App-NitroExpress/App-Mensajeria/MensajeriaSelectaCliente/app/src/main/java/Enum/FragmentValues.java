package Enum;

/**
 * Created by Daniel on 31/08/16.
 */
public class FragmentValues {

    public static final String ConsultaServicio = "ConsultaServicioController";
    public static final String EstimacionServicio = "EstimacionServicioController";
    public static final String Tarjeta = "TarjetaController";
    public static final String NuevaTarjeta = "NuevaTarjetaController";
    public static final String DetalleTarjeta = "DetalleTarjetaController";
    public static final String Historial = "HistorialController";
    public static final String Descuentos = "DescuentosController";
    public static final String Mispuntos = "MisPuntosController";
    public static final String Registro = "RegistroController";
    public static final String RestablecerClave = "RestablecerClaveController";
    public static final String Ayuda = "AyudaController";
    public static final String Configuracion = "Configuracionontroller";
    public static final String GoogleMaps = "mapsController";
    public static final String camaraVideo = "imagesVideosController";


}
