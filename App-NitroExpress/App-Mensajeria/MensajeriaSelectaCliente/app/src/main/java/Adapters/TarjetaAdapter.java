package Adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.util.ArrayList;
import java.util.List;

import Model.Tarjeta;

/**
 * Created by Daniel on 11/06/16.
 */
public class TarjetaAdapter extends ArrayAdapter<Tarjeta> {

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private List<Tarjeta> itemHistorialList;
    private List<Tarjeta> itemSeleccList;
    private Context currentContext;

    public TarjetaAdapter(Context context, List<Tarjeta> items, List<Tarjeta> itemsSel) {
        super(context, R.layout.lista_tarjetas_detalle, items);
        this.currentContext = context;
        this.itemSeleccList = itemsSel;
        this.itemHistorialList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) currentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lista_tarjetas_detalle, parent, false);
        }

        ImageView imageTargetImageView = (ImageView) convertView.findViewById(R.id.imageTargetImageView);
        TextView numberTargetTextView = (TextView) convertView.findViewById(R.id.numberTargetTextView);

        if (mSelection.contains(position)) {
            if(!mSelection.contains(position)){
                mSelection.add(position);
            }
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_verde));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Tarjeta tarjetaItem = itemHistorialList.get(position);

        imageTargetImageView.setImageResource(R.drawable.abc_ratingbar_indicator_material);

        numberTargetTextView.setText(tarjetaItem.getDescripcion());
        return convertView;
    }

    private Boolean contains(String numero_tarjeta, List<Tarjeta> itemSel){
        Boolean value = false;
        try {
            if (itemSel != null) {
                for (Tarjeta currentItem : itemSel) {
                    if (currentItem.getNumero().toString().equalsIgnoreCase(numero_tarjeta)) {
                        value = true;
                        return value;
                    }
                }
            }
        }catch (Exception ex){
            return false;
        }
        return value;
    }

    private static class ViewHolder {
        ImageView rutaImageView;
        ImageView profileCourierImageView;
        TextView precioTextView;
        TextView nameCourierTextView;
        TextView fechaServicioTextView;
        TextView nameCampaingTextView;
        TextView codeCampaingTextView;
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        String numero_tarjeta = itemHistorialList.get(position).getNumero().toString();
        if(mSelection.contains(position)){
            return true;
        }else if(contains(numero_tarjeta, itemSeleccList)) {
            mSelection.add(position);
            return true;
        }else{
            return false;
        }
    }

    public int containValuePosition(String numero_tarjeta, List<Tarjeta> itemSel){
        int value = -1;
        try {
            if (itemSel != null) {
                int position = 0;
                for (Tarjeta currentItem : itemSel) {
                    if (currentItem.getNumero().toString().equalsIgnoreCase(numero_tarjeta)) {
                        value = position;
                        return value;
                    }
                    position++;
                }
            }
        }catch (Exception ex){
            return -1;
        }
        return value;
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        String numero_tarjeta = itemHistorialList.get(position).getNumero().toString();
        if(contains(numero_tarjeta, itemSeleccList)) {
            int positionSel = containValuePosition(numero_tarjeta, itemSeleccList);
            itemSeleccList.remove(positionSel);
        }
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    public List<Tarjeta> getItemSeleccList() {
        return itemSeleccList;
    }
}
