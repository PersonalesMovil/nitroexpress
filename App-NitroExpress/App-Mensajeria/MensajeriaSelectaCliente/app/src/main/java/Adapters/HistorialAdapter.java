package Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.util.ArrayList;
import java.util.List;

import Model.Historial;

/**
 * Created by Daniel on 11/06/16.
 */
public class HistorialAdapter extends ArrayAdapter<Historial> {

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private List<Historial> itemHistorialList;
    private List<Historial> itemSeleccList;
    private Context currentContext;

    public HistorialAdapter(Context context, List<Historial> items, List<Historial> itemsSel) {
        super(context, R.layout.lista_historiales_detalle, items);
        this.currentContext = context;
        this.itemSeleccList = itemsSel;
        this.itemHistorialList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) currentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lista_historiales_detalle, parent, false);
        }

        ImageView rutaImageView = (ImageView) convertView.findViewById(R.id.rutaImageView);
        ImageView profileCourierImageView = (ImageView) convertView.findViewById(R.id.profileCourierImageView);
        TextView precioTextView = (TextView) convertView.findViewById(R.id.precioTextView);
        TextView nameCourierTextView = (TextView) convertView.findViewById(R.id.nameCourierTextView);
        TextView fechaServicioTextView = (TextView) convertView.findViewById(R.id.fechaServicioTextView);


        if (mSelection.contains(position)) {
            if(!mSelection.contains(position)){
                mSelection.add(position);
            }
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_verde));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Historial historialItem = itemHistorialList.get(position);
        rutaImageView.setImageBitmap(BitmapFactory.decodeFile(""));
        profileCourierImageView.setImageBitmap(historialItem.getProfileCourier());
        precioTextView.setText(historialItem.getTotalViaje());
        nameCourierTextView.setText(historialItem.getNameCourier());
        fechaServicioTextView.setText(historialItem.getFecha());

        return convertView;
    }

    private Boolean contains(String id_historial, List<Historial> itemSel){
        Boolean value = false;
        try {
            if (itemSel != null) {
                for (Historial currentItem : itemSel) {
                    if (currentItem.getId_historial().toString().equalsIgnoreCase(id_historial)) {
                        value = true;
                        return value;
                    }
                }
            }
        }catch (Exception ex){
            return false;
        }
        return value;
    }

    private static class ViewHolder {
        ImageView rutaImageView;
        ImageView profileCourierImageView;
        TextView precioTextView;
        TextView nameCourierTextView;
        TextView fechaServicioTextView;
        TextView nameCampaingTextView;
        TextView codeCampaingTextView;
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        String id_historial = itemHistorialList.get(position).getId_historial().toString();
        if(mSelection.contains(position)){
            return true;
        }else if(contains(id_historial, itemSeleccList)) {
            mSelection.add(position);
            return true;
        }else{
            return false;
        }
    }

    public int containValuePosition(String id_historial, List<Historial> itemSel){
        int value = -1;
        try {
            if (itemSel != null) {
                int position = 0;
                for (Historial currentItem : itemSel) {
                    if (currentItem.getId_historial().toString().equalsIgnoreCase(id_historial)) {
                        value = position;
                        return value;
                    }
                    position++;
                }
            }
        }catch (Exception ex){
            return -1;
        }
        return value;
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        String codeCampaing = itemHistorialList.get(position).getId_historial().toString();
        if(contains(codeCampaing, itemSeleccList)) {
            int positionSel = containValuePosition(codeCampaing, itemSeleccList);
            itemSeleccList.remove(positionSel);
        }
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    public List<Historial> getItemSeleccList() {
        return itemSeleccList;
    }

}
