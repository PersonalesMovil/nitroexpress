package Resource;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Daniel on 22/09/16.
 */
public class GoogleMapsApiQuery {

    public String getDirectionsUrl(LatLng origin, LatLng dest) {

        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    public String getDirectionUrl(LatLng location) {
        String str_location = "latlng=" + location.latitude + "," + location.longitude;
        String sensor = "sensor=true";
        String parameters = str_location + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/geocode/" + output + "?" + parameters;
        return url;
    }

    public String getDirectionPlacesURL(LatLng location, int radius, String types) {

        String str_location = "location=" + location.latitude + "," + location.longitude;
        String str_radius = "radius=" + radius;
        String str_types = "types=" + types;
        String parameters = str_location + "&" + str_radius + str_types;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/" + output + "?" + parameters;

        return url;
    }
}
