package Resource;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.selectacg.daniel.mensajeriaselectacliente.R;

import java.util.List;

/**
 * Created by SelectaCG on 28/08/15.
 */
public class LocationGPS{

    Activity current;
    LocationManager locationManager;

    public LocationGPS(Activity current) {
        this.current = current;
    }

    public LatLng getGPS() {
        LatLng gps = null;
        try {
            locationManager = (LocationManager) current.getSystemService(current.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
                return null;
            } else {
                LocationListener locationListener = new LocationListener() {

                    public void onLocationChanged(Location location) {
                    }

                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    public void onProviderEnabled(String provider) {
                    }

                    public void onProviderDisabled(String provider) {
                    }
                };
                //Registra el escuchador con el gestor de localizacion recibido en la actualizacion
                if (ActivityCompat.checkSelfPermission(current, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(current, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    return gps;
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                List<String> providers =  locationManager.getAllProviders();
                for (String currentProvider : providers) {
                    Location currentLocation = locationManager.getLastKnownLocation(currentProvider);
                    try {
                        if(currentProvider.equalsIgnoreCase("network") && currentLocation != null) {
                            gps = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        }
                        else if (currentProvider.equalsIgnoreCase("passive") && currentLocation != null && gps == null){
                            gps = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        }
                        else if (currentProvider.equalsIgnoreCase("gps") && currentLocation != null && gps == null){
                            gps = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                        }
                    }catch (Exception ex){
                        Log.d("Error GPS:", ex.getMessage());
                    }
                }
                return gps;
            }
        }catch(Exception ex){
            Log.d("Error GPS: ", ex.getMessage());
        }
        return gps;
    }

    private void buildAlertMessageNoGps() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(current);
            builder.setMessage(current.getString(R.string.confirmation_gps_desactived_wish_active))
                    .setCancelable(false);

            builder.setPositiveButton(current.getString(R.string.ctl_yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    current.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),123);
                }
            });

            builder.setNegativeButton(current.getString(R.string.ctl_no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }catch (Exception ex){
            Log.d("Error GPS", ex.getMessage());
        }
    }
}
