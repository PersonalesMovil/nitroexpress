package Model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;


public class Fotos implements Parcelable {

    //ID_INT de la tabla de posiciones ZTBILL_I_MRO, que es nuestra orden de lectura
    private String idInt;
     //Nombre del fichero de la foto
     private String nombreFichero;
    //String en formato Base64, es el que en teoría deben usar para pasarnos el binario del jpg de la foto
    private byte[] contenidoBase64;

    private Bitmap fotoBitMapFormat;

    public String currentPATH;

    public Fotos(){
        super();
    }

    public Fotos(Parcel in) {
        readFromParcel(in);
    }

    public Fotos(String idInt, String nombreFichero, byte[] contenidoBase64){
        super();
        this.idInt = idInt;
        this.nombreFichero = nombreFichero;
        this.contenidoBase64 = contenidoBase64;
    }

    public boolean isInitialized(){
        return !TextUtils.isEmpty(this.idInt)
                && !TextUtils.isEmpty(this.nombreFichero)
                && !TextUtils.isEmpty(this.contenidoBase64.toString());
    }

    public String getKeys(){
        String values = "";


        return values;
    }

    public Bitmap getFotoBitMapFormat() {
        return fotoBitMapFormat;
    }

    public void setFotoBitMapFormat(Bitmap fotoBitMapFormat) {
        this.fotoBitMapFormat = fotoBitMapFormat;
    }

    public String getIdInt() {
        return idInt;
    }

    public void setIdInt(String idInt) {
        this.idInt = idInt;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public byte[] getContenidoBase64() {
        return contenidoBase64;
    }

    public void setContenidoBase64(byte[] contenidoBase64) {
        this.contenidoBase64 = contenidoBase64;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nombreFichero);
        dest.writeString(this.idInt);
        dest.writeByteArray(this.contenidoBase64);
    }

    public void readFromParcel(Parcel in)
    {
        this.nombreFichero = in.readString();
        this.idInt = in.readString();
        this.contenidoBase64 = in.createByteArray();
    }

    public static final Parcelable.Creator<Fotos> CREATOR = new Parcelable.Creator<Fotos>()
    {
        @Override
        public Fotos createFromParcel(Parcel in)
        {
            return new Fotos(in);
        }

        @Override
        public Fotos[] newArray(int size)
        {
            return new Fotos[size];
        }
    };
}
