package Model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Daniel on 22/09/16.
 */
public class Direccion {

    public String direccion_origen;
    public String direccion_destino;
    public String distancia;
    public String tiempo_entre_distancia;
    public List<List<HashMap<String,String>>> coordenadas;
}
