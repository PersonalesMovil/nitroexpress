package Model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Daniel on 8/06/16.
 */
public class Ruta {
    public LatLng origen;
    public LatLng destino;
    String tiempoEstimado;
}
