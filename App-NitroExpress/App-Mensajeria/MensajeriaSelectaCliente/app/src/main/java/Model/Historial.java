package Model;

import android.graphics.Bitmap;

/**
 * Created by Daniel on 8/06/16.
 */
public class Historial {
    String id_historial;
    Ruta ruta;
    String calificacion;
    String totalViaje;
    String fecha;
    Tarjeta tarjetaCobro;
    String categoria;
    String nameCourier;
    Bitmap profileCourier;

    public String getId_historial() {
        return id_historial;
    }

    public void setId_historial(String id_historial) {
        this.id_historial = id_historial;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getTotalViaje() {
        return totalViaje;
    }

    public void setTotalViaje(String totalViaje) {
        this.totalViaje = totalViaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Tarjeta getTarjetaCobro() {
        return tarjetaCobro;
    }

    public void setTarjetaCobro(Tarjeta tarjetaCobro) {
        this.tarjetaCobro = tarjetaCobro;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNameCourier() {
        return nameCourier;
    }

    public void setNameCourier(String nameCourier) {
        this.nameCourier = nameCourier;
    }

    public Bitmap getProfileCourier() {
        return profileCourier;
    }

    public void setProfileCourier(Bitmap profileCourier) {
        this.profileCourier = profileCourier;
    }
}
